"""
    Setup file for s3toolext.
    Use setup.cfg to configure your project.

    This file was generated with PyScaffold 4.4.
    PyScaffold helps you to put up the scaffold of your new Python project.
    Learn more under: https://pyscaffold.org/
"""
from setuptools import setup

if __name__ == "__main__":
    try:
        setup(
            use_scm_version={"version_scheme": "no-guess-dev"},
            name='s3toolext',
            packages=[
                's3toolext', 
            ],
            entry_points={
                'console_scripts': [
                    's3toolext_outline_install=s3toolext.deploy:install_outline',
                    's3toolext_outline_addkeys=s3toolext.manage:add_outline_key',
                    's3toolext_outline_showkeys=s3toolext.manage:show_outline_keys',
                ]
            },
            install_requires="""
                faker
                typer
                """.splitlines(),
        )
    except:  # noqa
        print(
            "\n\nAn error occurred while building the project, "
            "please ensure you have the most updated version of setuptools, "
            "setuptools_scm and wheel with:\n"
            "   pip install -U setuptools setuptools_scm wheel\n\n"
        )
        raise
