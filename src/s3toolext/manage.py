import json
import uuid
import base64
import faker
from s3toolext.const import OUTLINE_SERVER_CONFIG_FILE, OUTLINE_KEYS_CONFIG_FILE
import pathlib
import os
import typer
import argparse



def start_outline():
    os.system('docker start watchtower')
    os.system('docker start shadowbox')


def stop_outline():
    os.system('docker stop watchtower')
    os.system('docker stop shadowbox')


def restart_outline():
    stop_outline()
    start_outline()


def write_outline_key():
    server_config_file = pathlib.Path(OUTLINE_SERVER_CONFIG_FILE)
    server_config_info = json.load(open(server_config_file))

    keys_config_file = pathlib.Path(OUTLINE_KEYS_CONFIG_FILE)
    keys_config_info = json.load(open(keys_config_file))

    next_key_id = keys_config_info.get('nextId', 1)
    access_keys = keys_config_info.get('accessKeys', [])  # type: list

    hostname = server_config_info['hostname']
    port = server_config_info['portForNewAccessKeys']
    new_password = uuid.uuid4().hex
    encrypt_method = 'chacha20-ietf-poly1305'

    method_password_info = encrypt_method+':'+new_password
    ss_url = 'ss://{b64info}@{hostname}:{port}/?outline=1'.format(
        b64info=base64.b64encode(method_password_info.encode('utf-8')).decode('utf-8'),
        hostname=hostname,
        port=port,
    )
    print()
    print(ss_url)
    print()

    key_json = {
        "id": "{}".format(next_key_id),
        "metricsId": uuid.uuid4().hex,
        "name": faker.Faker().name(),
        "password": new_password,
        "port": port,
        "encryptionMethod": encrypt_method
    }
    access_keys.append(key_json)
    keys_config_info['accessKeys'] = access_keys
    new_next_id = next_key_id + 1
    keys_config_info['nextId'] = new_next_id
    json.dump(keys_config_info, open(keys_config_file, 'w'))
    return keys_config_info

def add_outline_key():
    count = input('Input keys count to add: ').strip()
    while not count.isdigit():
        print('Please input a number.')
        count = input('Input keys count to add: ').strip()

    count = int(count)
    for i in range(count):
        write_outline_key()
    os.system('docker restart shadowbox')


def show_outline_keys():
    server_config_file = pathlib.Path(OUTLINE_SERVER_CONFIG_FILE)
    server_config_info = json.load(open(server_config_file))

    keys_config_file = pathlib.Path(OUTLINE_KEYS_CONFIG_FILE)
    keys_config_info = json.load(open(keys_config_file))

    hostname = server_config_info['hostname']
    port = server_config_info['portForNewAccessKeys']
    encrypt_method = 'chacha20-ietf-poly1305'

    access_keys = keys_config_info.get('accessKeys', [])  # type: list
    for ak in sorted(access_keys, key=lambda x: int(x['id'])):
        _id = ak['id']
        password = ak['password'] 
        name = ak['name']
        method_password_info = encrypt_method+':'+password
        ss_url = 'ss://{b64info}@{hostname}:{port}/?outline=1'.format(
            b64info=base64.b64encode(method_password_info.encode('utf-8')).decode('utf-8'),
            hostname=hostname,
            port=port,
        )
        print()
        print('#{} {}'.format(_id, name))
        print(ss_url)
        print()


