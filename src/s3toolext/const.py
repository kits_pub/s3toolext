OUTLINE_INSTALL_CMD = 'bash -c "$(wget -qO- https://raw.githubusercontent.com/Jigsaw-Code/outline-server/master/src/server_manager/install_scripts/install_server.sh)"'

OUTLINE_SERVER_CONFIG_FILE = '/opt/outline/persisted-state/shadowbox_server_config.json'

OUTLINE_KEYS_CONFIG_FILE = '/opt/outline/persisted-state/shadowbox_config.json'

