from s3toolext.const import OUTLINE_INSTALL_CMD, OUTLINE_SERVER_CONFIG_FILE, OUTLINE_KEYS_CONFIG_FILE
from subprocess import getstatusoutput, getoutput
import sys
import pathlib
import os


def install_outline():
    # status, output = getstatusoutput(OUTLINE_INSTALL_CMD)
    # if status != 0:
    #     print('Installed failed, please contact tech support.')
    #     print()
    #     sys.exit(407)

    os.system(OUTLINE_INSTALL_CMD)

    server_config_file = pathlib.Path(OUTLINE_SERVER_CONFIG_FILE)
    if not server_config_file.exists():
        print('Install uncompleted, please contact tech support.')
        print()
        sys.exit(409)

    keys_config_file = pathlib.Path(OUTLINE_KEYS_CONFIG_FILE)
    if not keys_config_file.exists():
        print('Start uncompleted, please contact tech support.')
        print()
        sys.exit(414)    
    

