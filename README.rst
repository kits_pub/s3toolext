.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://api.cirrus-ci.com/github/<USER>/s3toolext.svg?branch=main
        :alt: Built Status
        :target: https://cirrus-ci.com/github/<USER>/s3toolext
    .. image:: https://readthedocs.org/projects/s3toolext/badge/?version=latest
        :alt: ReadTheDocs
        :target: https://s3toolext.readthedocs.io/en/stable/
    .. image:: https://img.shields.io/coveralls/github/<USER>/s3toolext/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/s3toolext
    .. image:: https://img.shields.io/pypi/v/s3toolext.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/s3toolext/
    .. image:: https://img.shields.io/conda/vn/conda-forge/s3toolext.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/s3toolext
    .. image:: https://pepy.tech/badge/s3toolext/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/s3toolext
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/s3toolext

.. image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
    :alt: Project generated with PyScaffold
    :target: https://pyscaffold.org/

|

=========
s3toolext
=========


    Add a short description here!


A longer description of your project goes here...


.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.4. For details and usage
information on PyScaffold see https://pyscaffold.org/.
